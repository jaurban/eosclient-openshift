FROM cern/cc7-base

# Volume where to mount the keytab as a secrets
# If credentials are passed as username and password with
# KEYTAB_USER and KEYTAB_PWD environment variables, a keytab will be
# generated and stored in KEYTAB_PATH.
ENV KEYTAB_PATH '/var/lib/secrets'
# EOS credentials, this will be shared from the host
ENV EOS_CREDENTIALS_PATH '/var/jenkins_home/credentials'

VOLUME ["${EOS_CREDENTIALS_PATH}"]

# Add EOS rpm repository to install necessary EOS related packages
COPY eos.repo /etc/yum.repos.d/

# Install all necessary packages to run eosfusebind and the renewal of the kerberos ticket
RUN yum install -y eos-fuse kstart krb5-workstation && yum clean all

COPY run.sh /

RUN mkdir -p $KEYTAB_PATH && chmod a+rw $KEYTAB_PATH && chmod a+x /run.sh
RUN mkdir -p $EOS_CREDENTIALS_PATH && chmod a+rw $EOS_CREDENTIALS_PATH

ENTRYPOINT ["/run.sh"]
